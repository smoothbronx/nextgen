import { PowerSupplyEntity } from '@/components/submodules/powerSupplies/powerSupply.entity';
import { MotherboardEntity } from '@/components/submodules/motherboards/motherboard.entity';
import { ComponentEntityBase } from '@/components/submodules/base/component.entity.base';
import { VideoCardEntity } from '@/components/submodules/videoCards/videoCard.entity';
import { ProcessorEntity } from '@/components/submodules/processors/processor.entity';
import { HardDriveEntity } from '@/components/submodules/hardDrives/hardDrive.entity';
import { RamMemoryEntity } from '@/components/submodules/ramMemory/ramMemory.entity';
import { AssemblyHardDriveEntity } from '@/assemblies/entities/hards.entity';
import { AssemblyEntity } from '@/assemblies/entities/assembly.entity';
import { AssemblyRamEntity } from '@/assemblies/entities/ram.entity';
import { AssemblyDto } from '@/assemblies/assembly.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '@/users/user.entity';
import { IsNull, Repository } from 'typeorm';
import {
    BadRequestException,
    NotFoundException,
    Injectable,
} from '@nestjs/common';

@Injectable()
export class AssembliesService {
    constructor(
        @InjectRepository(AssemblyEntity)
        private readonly assembliesRepository: Repository<AssemblyEntity>,
        @InjectRepository(MotherboardEntity)
        private readonly motherboardsRepository: Repository<MotherboardEntity>,
        @InjectRepository(PowerSupplyEntity)
        private readonly powerSuppliesRepository: Repository<PowerSupplyEntity>,
        @InjectRepository(HardDriveEntity)
        private readonly hardDrivesRepository: Repository<HardDriveEntity>,
        @InjectRepository(ProcessorEntity)
        private readonly processorsRepository: Repository<ProcessorEntity>,
        @InjectRepository(RamMemoryEntity)
        private readonly ramMemoriesRepository: Repository<RamMemoryEntity>,
        @InjectRepository(VideoCardEntity)
        private readonly videoCardsRepository: Repository<VideoCardEntity>,
        @InjectRepository(AssemblyRamEntity)
        private readonly assemblyRamRepository: Repository<AssemblyRamEntity>,
        @InjectRepository(AssemblyHardDriveEntity)
        private readonly assemblyHardsRepository: Repository<AssemblyHardDriveEntity>,
    ) {}

    public getAssemblies() {
        return this.assembliesRepository.findBy({ author: IsNull() });
    }

    public async getAssembly(id: number): Promise<AssemblyEntity> {
        const assembly = await this.assembliesRepository.findOneBy({ id });

        if (!assembly) {
            throw new NotFoundException('Assembly not found');
        }

        return assembly;
    }

    public async deleteAssembly(id: number): Promise<void> {
        const assembly = await this.getAssembly(id);
        await assembly.remove();
    }

    public async createAssembly(
        user: UserEntity,
        assembly: AssemblyDto,
    ): Promise<AssemblyEntity> {
        const motherboard: MotherboardEntity = await this.getComponent(
            this.motherboardsRepository,
            assembly.motherboardId,
            'Motherboard',
        );

        const processor: ProcessorEntity = await this.getComponent(
            this.processorsRepository,
            assembly.processorId,
            'Processor',
        );

        if (motherboard.socket !== processor.socket) {
            throw new BadRequestException(
                'Процессор и материнская плата несовместимы по сокету',
            );
        }

        const powerSupply = await this.getComponent(
            this.powerSuppliesRepository,
            assembly.powerSupplyId,
            'Power Supply',
        );

        const videoCard = await this.getComponent(
            this.videoCardsRepository,
            assembly.videoCardId,
            'Video card',
        );

        const ramMemory: RamMemoryEntity[] = await this.getComponents(
            assembly.ramMemoryIds,
            this.ramMemoriesRepository,
            'Ram memory',
        );

        const totalRamMemory = ramMemory
            .map((ram) => ram.capacity)
            .reduce((p, n) => p + n);

        if (totalRamMemory > motherboard.maxMemoryCapacity) {
            throw new BadRequestException(
                'Объект памяти ОЗУ превышает допустимое количество',
            );
        }
        if (ramMemory.length > motherboard.ramQuantity) {
            throw new BadRequestException(
                'Количество плашек ОЗУ больше количества слотов на материнской плате',
            );
        }
        const assemblyRam: AssemblyRamEntity[] = await this.getAssemblyRam(
            ramMemory,
        );

        const hardDrives: HardDriveEntity[] = await this.getComponents(
            assembly.hardDrivesIds,
            this.hardDrivesRepository,
            'Hard drive',
        );

        const assemblyHards: AssemblyHardDriveEntity[] =
            await this.getAssemblyHards(hardDrives);

        const assemblyEnt = await this.assembliesRepository
            .create({
                author: { id: user.id },
                ramMemory: assemblyRam,
                hardDrives: assemblyHards,
                name: assembly.name,
                processor,
                videoCard,
                motherboard,
                powerSupply,
            })
            .save();

        return this.assembliesRepository.findOneById(
            assemblyEnt.id,
        ) as Promise<AssemblyEntity>;
    }

    public async getAssemblyRam(
        rams: RamMemoryEntity[],
    ): Promise<AssemblyRamEntity[]> {
        const ids = rams.map((ram) => ram.id);
        const uniquesId = [...new Set(ids)];

        const result: AssemblyRamEntity[] = [];
        for (const id of uniquesId) {
            result.push(
                await this.assemblyRamRepository
                    .create({
                        ram: { id },
                        quantity: ids.filter((relId) => relId === id).length,
                    })
                    .save(),
            );
        }
        return result;
    }

    public async getAssemblyHards(rams: HardDriveEntity[]) {
        const ids = rams.map((ram) => ram.id);
        const uniquesId = [...new Set(ids)];

        const result: AssemblyHardDriveEntity[] = [];
        for (const id of uniquesId) {
            result.push(
                await this.assemblyHardsRepository
                    .create({
                        hards: { id },
                        quantity: ids.filter((relId) => relId === id).length,
                    })
                    .save(),
            );
        }
        return result;
    }

    private async getComponents<Component extends ComponentEntityBase>(
        ids: number[],
        repository: any,
        name: string,
    ) {
        const result: Component[] = [];
        for (const id of ids) {
            result.push(await this.getComponent(repository, id, name));
        }
        return result;
    }

    private async getComponent<Component extends ComponentEntityBase>(
        repository: any,
        id: number,
        name: string,
    ): Promise<Component> {
        const component = await repository.findOneBy({ id });

        if (!component) {
            throw new NotFoundException(`${name} with id ${id} not found`);
        }

        return component;
    }
}
