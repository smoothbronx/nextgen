import { HardDriveEntity } from '@/components/submodules/hardDrives/hardDrive.entity';
import { AssemblyEntity } from '@/assemblies/entities/assembly.entity';
import { Exclude } from 'class-transformer';
import {
    PrimaryGeneratedColumn,
    BaseEntity,
    JoinColumn,
    ManyToOne,
    Column,
    Entity,
} from 'typeorm';

@Entity('assemblies_hards')
export class AssemblyHardDriveEntity extends BaseEntity {
    @Exclude({ toPlainOnly: true })
    @PrimaryGeneratedColumn()
    public id: number;

    @JoinColumn({ name: 'assembly_id' })
    @ManyToOne(() => AssemblyEntity, (assembly) => assembly.hardDrives, {
        onDelete: 'CASCADE',
        nullable: true,
    })
    public assembly: AssemblyEntity;

    @JoinColumn({ name: 'hard_id' })
    @ManyToOne(() => HardDriveEntity, {
        onDelete: 'CASCADE',
        nullable: false,
        eager: true,
    })
    public hards: HardDriveEntity;

    @Column({ name: 'quantity', nullable: false, default: 1 })
    public quantity: number;
}
