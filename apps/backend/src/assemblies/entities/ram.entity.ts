import { RamMemoryEntity } from '@/components/submodules/ramMemory/ramMemory.entity';
import { AssemblyEntity } from '@/assemblies/entities/assembly.entity';
import { Exclude } from 'class-transformer';
import {
    PrimaryGeneratedColumn,
    BaseEntity,
    JoinColumn,
    ManyToOne,
    Column,
    Entity,
} from 'typeorm';

@Entity('assemblies_ram')
export class AssemblyRamEntity extends BaseEntity {
    @Exclude({ toPlainOnly: true })
    @PrimaryGeneratedColumn()
    public id: number;

    @JoinColumn({ name: 'assembly_id' })
    @ManyToOne(() => AssemblyEntity, (assembly) => assembly.ramMemory, {
        onDelete: 'CASCADE',
        nullable: true,
    })
    public assembly: AssemblyEntity | null;

    @JoinColumn({ name: 'ram_id' })
    @ManyToOne(() => RamMemoryEntity, {
        onDelete: 'CASCADE',
        nullable: false,
        eager: true,
    })
    public ram: RamMemoryEntity;

    @Column({ name: 'quantity', nullable: false, default: 1 })
    public quantity: number;
}
