import { PowerSupplyEntity } from '@/components/submodules/powerSupplies/powerSupply.entity';
import { MotherboardEntity } from '@/components/submodules/motherboards/motherboard.entity';
import { ProcessorEntity } from '@/components/submodules/processors/processor.entity';
import { VideoCardEntity } from '@/components/submodules/videoCards/videoCard.entity';
import { HardDriveEntity } from '@/components/submodules/hardDrives/hardDrive.entity';
import { RamMemoryEntity } from '@/components/submodules/ramMemory/ramMemory.entity';
import { UserEntity } from '@/users/user.entity';
import { Exclude, Expose } from 'class-transformer';
import {
    PrimaryGeneratedColumn,
    BaseEntity,
    ManyToMany,
    JoinColumn,
    ManyToOne,
    JoinTable,
    Column,
    Entity,
} from 'typeorm';
import { AssemblyRamEntity } from '@/assemblies/entities/ram.entity';
import { AssemblyHardDriveEntity } from '@/assemblies/entities/hards.entity';

@Entity('assemblies')
export class AssemblyEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ name: 'name', type: 'varchar', nullable: false })
    public name: string;

    @JoinColumn()
    @ManyToOne(() => ProcessorEntity, {
        onDelete: 'CASCADE',
        nullable: false,
        eager: true,
    })
    public processor: ProcessorEntity;

    @JoinColumn()
    @ManyToOne(() => VideoCardEntity, {
        onDelete: 'CASCADE',
        nullable: false,
        eager: true,
    })
    public videoCard: VideoCardEntity;

    @JoinColumn()
    @ManyToOne(() => MotherboardEntity, {
        onDelete: 'CASCADE',
        nullable: false,
        eager: true,
    })
    public motherboard: MotherboardEntity;

    @Exclude({ toPlainOnly: true })
    @JoinTable()
    @ManyToMany(
        () => AssemblyRamEntity,
        (assemblyRam) => assemblyRam.assembly,
        {
            nullable: false,
            cascade: true,
            eager: true,
        },
    )
    public ramMemory: AssemblyRamEntity[];

    @Exclude({ toPlainOnly: true })
    @JoinTable()
    @ManyToMany(
        () => AssemblyHardDriveEntity,
        (assemblyHard) => assemblyHard.assembly,
        {
            onDelete: 'CASCADE',
            nullable: false,
            eager: true,
        },
    )
    public hardDrives: AssemblyHardDriveEntity[];

    @JoinColumn()
    @ManyToOne(() => PowerSupplyEntity, {
        onDelete: 'CASCADE',
        nullable: false,
        eager: true,
    })
    public powerSupply: PowerSupplyEntity;

    @JoinColumn()
    @ManyToOne(() => UserEntity, {
        onDelete: 'CASCADE',
        nullable: true,
    })
    public author: UserEntity | null;

    @Expose({ toPlainOnly: true, name: 'hardDrives' })
    public getHardDrives(): HardDriveEntity[] {
        const result: HardDriveEntity[] = [];
        this.hardDrives.map((hardDrive) => {
            for (let i = 0; i < hardDrive.quantity; i++) {
                result.push(hardDrive.hards);
            }
        });
        return result;
    }

    @Expose({ toPlainOnly: true, name: 'ramMemory' })
    public getRam(): RamMemoryEntity[] {
        const result: RamMemoryEntity[] = [];
        this.ramMemory.map((ram) => {
            for (let i = 0; i < ram.quantity; i++) {
                result.push(ram.ram);
            }
        });

        return result;
    }

    @Expose({ toPlainOnly: true })
    public price(): number {
        return (
            this.processor.price +
            this.videoCard.price +
            this.motherboard.price +
            this.ramMemory
                .map((ram) => ram.ram.price * ram.quantity)
                .reduce((prev, next) => prev + next) +
            this.hardDrives
                .map((hardDrive) => hardDrive.hards.price * hardDrive.quantity)
                .reduce((prev, next) => prev + next) +
            this.powerSupply.price
        );
    }
}
