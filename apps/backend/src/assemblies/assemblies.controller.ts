import { ApiException } from '@nanogiants/nestjs-swagger-api-exception-decorator';
import { AssembliesService } from '@/assemblies/assemblies.service';
import { AssemblyEntity } from '@/assemblies/entities/assembly.entity';
import { AssemblyDto } from '@/assemblies/assembly.dto';
import { User } from 'shared/handlers/user.handler';
import { JwtAuthGuard } from 'shared/jwt/jwt.guard';
import { UserEntity } from '@/users/user.entity';
import {
    UnauthorizedException,
    BadRequestException,
    NotFoundException,
    ParseIntPipe,
    Controller,
    HttpStatus,
    UseGuards,
    HttpCode,
    Delete,
    Inject,
    Param,
    Body,
    Get,
    Post,
} from '@nestjs/common';
import {
    ApiNoContentResponse,
    ApiCreatedResponse,
    ApiOkResponse,
    ApiOperation,
    ApiHeader,
    ApiParam,
    ApiBody,
    ApiTags,
} from '@nestjs/swagger';

@ApiHeader({
    name: 'Authorization',
    description: 'Bearer access token',
    example: 'Bearer <accessToken>',
    required: true,
})
@ApiException(() => new UnauthorizedException('Invalid access token'), {
    description: 'Пользователь поставил некорректный токен доступа',
})
@ApiTags('Сборки')
@UseGuards(JwtAuthGuard)
@Controller('/assemblies/')
export class AssembliesController {
    constructor(
        @Inject(AssembliesService)
        private readonly assembliesService: AssembliesService,
    ) {}

    @ApiOkResponse({
        type: AssemblyDto,
        isArray: true,
    })
    @ApiOperation({ summary: 'Получение всех сборок' })
    @Get('/')
    public getAssemblies(): Promise<AssemblyEntity[]> {
        return this.assembliesService.getAssemblies();
    }

    @ApiOperation({ summary: 'Получение конкретной сборки' })
    @ApiException(() => new NotFoundException('Assembly not found'), {
        description: 'Сборка не найдена',
    })
    @ApiOkResponse({ type: AssemblyDto })
    @ApiParam({
        name: 'assemblyId',
        type: Number,
    })
    @Get('/:assemblyId/')
    public getAssembly(
        @Param('assemblyId', ParseIntPipe) id: number,
    ): Promise<AssemblyEntity> {
        return this.assembliesService.getAssembly(id);
    }

    @ApiOperation({ summary: 'Удаление конкретной сборки' })
    @ApiException(() => new NotFoundException('Assembly not found'), {
        description: 'Сборка не найдена',
    })
    @ApiNoContentResponse({ type: AssemblyDto })
    @ApiParam({
        name: 'assemblyId',
        type: Number,
    })
    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete('/:assemblyId/')
    public deleteAssembly(
        @Param('assemblyId', ParseIntPipe) id: number,
    ): Promise<void> {
        return this.assembliesService.deleteAssembly(id);
    }

    @ApiException(
        () =>
            new BadRequestException(
                'Количество ОЗУ больше количества слотов на материнской плате',
            ),
    )
    @ApiException(
        () =>
            new BadRequestException(
                'Объект памяти ОЗУ превышает допустимое количество',
            ),
    )
    @ApiException(
        () =>
            new BadRequestException(
                'Процессор и материнская плата несовместимы по сокету',
            ),
    )
    @ApiException(
        () => new NotFoundException(`{componentName} with id {id} not found`),
        {
            description: 'Компонент типа {componentName} не был найден по {id}',
        },
    )
    @ApiBody({ type: AssemblyDto })
    @ApiCreatedResponse({ type: AssemblyDto })
    @Post('/')
    public async createAssembly(
        @User() client: UserEntity,
        @Body() assembly: AssemblyDto,
    ) {
        return this.assembliesService.createAssembly(client, assembly);
    }
}
