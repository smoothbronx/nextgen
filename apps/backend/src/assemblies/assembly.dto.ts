import { ApiProperty } from '@nestjs/swagger';
import { PowerSupplyDto } from '@/components/submodules/powerSupplies/powerSupply.dto';
import { HardDriveDto } from '@/components/submodules/hardDrives/hardDrive.dto';
import { RamMemoryDto } from '@/components/submodules/ramMemory/ramMemory.dto';
import { MotherboardDto } from '@/components/submodules/motherboards/motherboard.dto';
import { VideoCardDto } from '@/components/submodules/videoCards/videoCard.dto';
import { ProcessorDto } from '@/components/submodules/processors/processor.dto';

export class AssemblyDto {
    @ApiProperty({
        name: 'id',
        description: 'Идентификатор сборки',
        nullable: false,
        readOnly: true,
    })
    public id: number;

    @ApiProperty({
        name: 'name',
        description: 'Имя сборки',
        required: true,
    })
    public name: string;

    @ApiProperty({
        name: 'processorId',
        description: 'Идентификатор существующего блока питания',
        writeOnly: true,
        required: true,
    })
    public readonly processorId: number;

    @ApiProperty({
        name: 'videoCardId',
        description: 'Идентификатор существующей видеокарты',
        writeOnly: true,
        required: true,
    })
    public readonly videoCardId: number;

    @ApiProperty({
        name: 'motherboardId',
        description: 'Идентификатор существующей материнской платы',
        writeOnly: true,
        required: true,
    })
    public readonly motherboardId: number;

    @ApiProperty({
        name: 'ramMemoryIds',
        description: 'Идентификаторы существующих хранилищ оперативной памяти',
        writeOnly: true,
        nullable: false,
        required: true,
        isArray: true,
    })
    public readonly ramMemoryIds: number[];

    @ApiProperty({
        name: 'hardDrivesIds',
        description: 'Идентификаторы существующих жестких дисков',
        writeOnly: true,
        nullable: false,
        required: true,
        isArray: true,
    })
    public readonly hardDrivesIds: number[];

    @ApiProperty({
        name: 'powerSupplyId',
        description: 'Идентификатор существующего блока питания',
        writeOnly: true,
        required: true,
    })
    public readonly powerSupplyId: number;

    @ApiProperty({
        name: 'processor',
        description: 'Объект процессора',
        nullable: false,
        readOnly: true,
    })
    public processor: ProcessorDto;

    @ApiProperty({
        name: 'videoCard',
        description: 'Идентификатор существующей видеокарты',
        type: VideoCardDto,
        nullable: false,
        readOnly: true,
    })
    public videoCard: VideoCardDto;

    @ApiProperty({
        name: 'motherboard',
        description: 'Объект материнской платы',
        type: MotherboardDto,
        nullable: false,
        readOnly: true,
    })
    public motherboard: MotherboardDto;

    @ApiProperty({
        name: 'ramMemories',
        description: 'Объекты хранилищ оперативной памяти',
        type: RamMemoryDto,
        nullable: false,
        readOnly: true,
        isArray: true,
    })
    public ramMemories: RamMemoryDto[];

    @ApiProperty({
        name: 'hardDrives',
        description: 'Объекты жестких дисков',
        type: HardDriveDto,
        nullable: false,
        readOnly: true,
        isArray: true,
    })
    public hardDrives: HardDriveDto[];

    @ApiProperty({
        name: 'powerSupply',
        description: 'Объект блока питания',
        type: PowerSupplyDto,
        nullable: false,
        readOnly: true,
    })
    public powerSupply: PowerSupplyDto;
}
