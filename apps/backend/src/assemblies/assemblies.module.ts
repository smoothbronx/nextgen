import { AssemblyHardDriveEntity } from '@/assemblies/entities/hards.entity';
import { AssembliesController } from '@/assemblies/assemblies.controller';
import { AssemblyEntity } from '@/assemblies/entities/assembly.entity';
import { AssemblyRamEntity } from '@/assemblies/entities/ram.entity';
import { AssembliesService } from '@/assemblies/assemblies.service';
import { ComponentsModule } from '@/components/components.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';

const dynamicTypeormModule = TypeOrmModule.forFeature([
    AssemblyEntity,
    AssemblyRamEntity,
    AssemblyHardDriveEntity,
]);

@Module({
    imports: [dynamicTypeormModule, ComponentsModule],
    providers: [AssembliesService],
    controllers: [AssembliesController],
    exports: [dynamicTypeormModule, AssembliesService],
})
export class AssembliesModule {}
