import { ApiException } from '@nanogiants/nestjs-swagger-api-exception-decorator';
import { VerificationsService } from 'src/verifications/verifications.service';
import { VerificationDto } from '@/verifications/dto/verification.dto';
import { EmailDto } from '@/verifications/dto/email.dto';
import {
    MethodNotAllowedException,
    BadRequestException,
    NotFoundException,
    Controller,
    HttpStatus,
    HttpCode,
    Inject,
    Body,
    Post,
} from '@nestjs/common';
import {
    ApiNoContentResponse,
    ApiCreatedResponse,
    ApiOperation,
    ApiBody,
    ApiTags,
} from '@nestjs/swagger';

@ApiTags('Верификация')
@Controller('/auth/verification/')
export class VerificationsController {
    constructor(
        @Inject(VerificationsService)
        private readonly verificationsService: VerificationsService,
    ) {}

    @ApiBody({
        type: EmailDto,
    })
    @ApiCreatedResponse({
        description: 'Отправка сообщения прошла успешно',
    })
    @ApiException(
        () =>
            new MethodNotAllowedException(
                'The user does not need verification',
            ),
        {
            description:
                'Пользователь уже верифицирован, дополнительная верификация не требуется',
        },
    )
    @ApiException(() => new NotFoundException('User not found'), {
        description: 'Пользователя с указанной почтой не существует',
    })
    @ApiOperation({ summary: 'Повторение отправки кода' })
    @HttpCode(HttpStatus.CREATED)
    @Post('/resend/')
    public async resendVerification(@Body() credentials: EmailDto) {
        await this.verificationsService.createVerification(credentials.email);
    }

    @ApiBody({
        type: VerificationDto,
    })
    @ApiNoContentResponse({
        description:
            'Верификация прошла успешно и пользователь может быть авторизован в системе',
    })
    @ApiException(() => new BadRequestException('Invalid verification code'), {
        description: 'Некорректный код подтверждения',
    })
    @ApiOperation({ summary: 'Подтверждение почты' })
    @HttpCode(HttpStatus.NO_CONTENT)
    @Post('/verify/')
    public verifyCode(@Body() verificationDto: VerificationDto) {
        return this.verificationsService.verifyCode(verificationDto);
    }
}
