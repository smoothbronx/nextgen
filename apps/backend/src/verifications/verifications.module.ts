import { VerificationsController } from '@/verifications/verifications.controller';
import { VerificationsService } from '@/verifications/verifications.service';
import { VerificationEntity } from '@/verifications/verification.entity';
import { forwardRef, Module } from '@nestjs/common';
import { UsersModule } from '@/users/users.module';
import { MailsModule } from '@/mails/mails.module';
import { TypeOrmModule } from '@nestjs/typeorm';

const dynamicTypeormModule = TypeOrmModule.forFeature([VerificationEntity]);

@Module({
    imports: [dynamicTypeormModule, forwardRef(() => UsersModule), MailsModule],
    providers: [VerificationsService],
    controllers: [VerificationsController],
    exports: [dynamicTypeormModule, VerificationsService],
})
export class VerificationsModule {}
