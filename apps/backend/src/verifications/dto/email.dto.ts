import { ApiProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';

export class EmailDto {
    @IsEmail()
    @ApiProperty({
        name: 'email',
        description: 'Адрес электронной почты',
        writeOnly: true,
        required: true,
    })
    public email: string;
}
