import { VerificationEntity } from 'src/verifications/verification.entity';
import { VerificationDto } from '@/verifications/dto/verification.dto';
import { UsersService } from '@/users/users.service';
import { MailsService } from '@/mails/mails.service';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '@/users/user.entity';
import { Repository } from 'typeorm';
import {
    MethodNotAllowedException,
    BadRequestException,
    NotFoundException,
    Injectable,
    Inject,
} from '@nestjs/common';

@Injectable()
export class VerificationsService {
    constructor(
        @InjectRepository(VerificationEntity)
        private readonly verificationsRepository: Repository<VerificationEntity>,
        @Inject(UsersService)
        private readonly usersService: UsersService,
        @Inject(MailsService)
        private readonly mailsService: MailsService,
    ) {}

    public async createVerification(
        email: string,
        user?: UserEntity,
    ): Promise<VerificationEntity> {
        if (!user) {
            const nullableUser = await this.usersService.getUserByEmail(email);
            if (!nullableUser) {
                throw new NotFoundException('User not found');
            }
            user = nullableUser;
        }

        if (user.isActive) {
            throw new MethodNotAllowedException(
                'The user does not need verification',
            );
        }

        await this.verificationsRepository.delete({ user: { id: user.id } });

        const code = Math.floor((1 + Math.random() * 9) * 10000000);

        const verification = await this.verificationsRepository.create({
            email,
            user,
            code,
        });

        await this.sendVerification(user.email, code);
        return verification.save();
    }

    public async verifyCode(credentials: VerificationDto): Promise<void> {
        const verification = await this.verificationsRepository.findOneBy({
            email: credentials.email,
        });

        if (!verification) {
            throw new MethodNotAllowedException(
                'The user does not need verification',
            );
        }

        if (verification.code !== credentials.code) {
            throw new BadRequestException('Invalid verification code');
        }

        const user = verification.user;
        user.isActive = true;
        await user.save();
        await verification.remove();
    }

    public async sendVerification(
        receiver: string,
        code: number,
    ): Promise<void> {
        await this.mailsService.sendMail(
            receiver,
            'Подтверждение почты',
            code.toString(),
        );
    }
}
