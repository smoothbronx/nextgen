import { ProductDto } from '@/products/dto/product.dto';
import { AssemblyDto } from '@/assemblies/assembly.dto';
import { ApiProperty } from '@nestjs/swagger';

export class OrderItemDto {
    @ApiProperty({
        description: 'Идентификатор элемента',
        readOnly: true,
    })
    public id: number;

    @ApiProperty({
        description: 'Объект продукта',
        readOnly: true,
        required: false,
    })
    public product?: ProductDto;

    @ApiProperty({
        description: 'Объект сборки',
        readOnly: true,
        required: false,
    })
    public assembly?: AssemblyDto;

    @ApiProperty({
        description: 'Индикация является ли объект сборкой',
        readOnly: true,
    })
    public isAssembly: boolean;

    @ApiProperty({
        description: 'Количество объектов',
        readOnly: true,
    })
    public quantity: number;

    @ApiProperty({
        description: 'Цена позиции',
        readOnly: true,
    })
    public price: number;
}
