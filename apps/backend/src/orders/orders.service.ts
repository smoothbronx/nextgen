import {
    ForbiddenException,
    Injectable,
    MethodNotAllowedException,
    NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderItemEntity } from '@/orders/entities/item.entity';
import { In, Not, Repository } from 'typeorm';
import { OrderEntity } from '@/orders/entities/order.entity';
import { UserEntity } from '@/users/user.entity';
import { Roles } from 'types/Roles';
import { Statuses } from '@/orders/Statuses';
import { OrderStatusDto } from '@/orders/dto/status.dto';
import { CartItemsIdsDto } from '@/orders/dto/cartItemsIds.dto';
import { CartItemEntity } from '@/carts/entities/item.entity';

@Injectable()
export class OrdersService {
    constructor(
        @InjectRepository(OrderItemEntity)
        private readonly orderItemsRepository: Repository<OrderItemEntity>,
        @InjectRepository(OrderEntity)
        private readonly ordersRepository: Repository<OrderEntity>,
        @InjectRepository(CartItemEntity)
        private readonly cartItemsRepository: Repository<CartItemEntity>,
    ) {}

    public getOrders(user: UserEntity) {
        if (user.role === Roles.ADMIN) {
            return this.ordersRepository.findBy({
                status: Not(Statuses.Rejected),
            });
        }
        return this.ordersRepository.findBy({
            client: { id: user.id },
        });
    }

    public async getOrder(
        user: UserEntity,
        orderId: number,
    ): Promise<OrderEntity> {
        let order: OrderEntity | null;
        if (user.role === Roles.ADMIN) {
            order = await this.ordersRepository.findOneById(orderId);
        } else {
            order = await this.ordersRepository.findOneBy({
                client: { id: user.id },
                id: orderId,
            });
        }

        if (!order) {
            throw new NotFoundException('Order not found');
        }

        return order;
    }

    public async createOrder(
        user: UserEntity,
        data: CartItemsIdsDto,
    ): Promise<OrderEntity> {
        if (user.role === Roles.ADMIN) {
            throw new MethodNotAllowedException();
        }

        const order = this.ordersRepository.create({
            status: Statuses.Created,
            client: { id: user.id },
            items: await this.transformItems(
                await this.getCartItemsByIds(user, data.ids),
            ),
        });

        return order.save();
    }

    private async transformItems(
        items: CartItemEntity[],
    ): Promise<OrderItemEntity[]> {
        const result: OrderItemEntity[] = [];
        for (const item of items) {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const { cart, id, ...generalData } = item;
            result.push(
                await this.orderItemsRepository.create(generalData).save(),
            );
            await item.remove();
        }
        return result;
    }

    private getCartItemsByIds(
        user: UserEntity,
        itemsIds?: number[],
    ): Promise<CartItemEntity[]> {
        return this.cartItemsRepository.findBy({
            cart: { client: { id: user.id } },
            id: itemsIds && itemsIds.length !== 0 ? In(itemsIds) : undefined,
        });
    }

    public async changeOrderStatus(
        user: UserEntity,
        orderId: number,
        data: OrderStatusDto,
    ): Promise<OrderEntity> {
        if (user.role !== Roles.ADMIN) {
            throw new ForbiddenException(
                'You dont have enough rights to perform this operation',
            );
        }

        const order = await this.ordersRepository.findOneById(orderId);
        if (!order) {
            throw new NotFoundException('Order not found');
        }

        order.status = data.status;
        return order.save();
    }
}
