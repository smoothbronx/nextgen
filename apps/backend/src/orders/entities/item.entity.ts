import { PrimaryGeneratedColumn, JoinColumn, ManyToOne, Column } from 'typeorm';
import { AssemblyEntity } from '@/assemblies/entities/assembly.entity';
import { OrderEntity } from '@/orders/entities/order.entity';
import { ProductEntity } from '@/products/product.entity';
import { BaseEntity, Entity } from 'typeorm';
import { Expose } from 'class-transformer';

@Entity('order_items')
export class OrderItemEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @JoinColumn({ name: 'order_id' })
    @ManyToOne(() => OrderEntity, (order) => order.items, {
        onDelete: 'CASCADE',
        nullable: true,
    })
    public order: OrderEntity | null;

    @JoinColumn({ name: 'product_id' })
    @ManyToOne(() => ProductEntity, {
        onDelete: 'CASCADE',
        nullable: true,
        eager: true,
    })
    public product: ProductEntity | null;

    @JoinColumn({ name: 'assembly_id' })
    @ManyToOne(() => AssemblyEntity, {
        onDelete: 'CASCADE',
        nullable: true,
        eager: true,
    })
    public assembly: AssemblyEntity | null;

    @Column({ name: 'is_assembly', type: 'boolean', nullable: false })
    public isAssembly: boolean;

    @Column({ name: 'quantity', nullable: false, default: 1 })
    public quantity: number;

    @Expose({ toPlainOnly: true })
    public price() {
        if (this.isAssembly && this.assembly) {
            return this.assembly.price() * this.quantity;
        }

        if (this.product) {
            return this.product.price() * this.quantity;
        }

        return 0;
    }
}
