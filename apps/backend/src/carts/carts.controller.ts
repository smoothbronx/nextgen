import { ApiException } from '@nanogiants/nestjs-swagger-api-exception-decorator';
import { CartEntity } from '@/carts/entities/cart.entity';
import { CartsService } from '@/carts/carts.service';
import { User } from 'shared/handlers/user.handler';
import { JwtAuthGuard } from 'shared/jwt/jwt.guard';
import { UserEntity } from '@/users/user.entity';
import { QuantityDto } from './dto/quantity.dto';
import { CartDto } from '@/carts/dto/cart.dto';
import {
    ApiCreatedResponse,
    ApiOkResponse,
    ApiOperation,
    ApiHeader,
    ApiParam,
    ApiBody,
    ApiTags,
} from '@nestjs/swagger';
import {
    UnauthorizedException,
    NotFoundException,
    ParseIntPipe,
    Controller,
    HttpStatus,
    UseGuards,
    HttpCode,
    Delete,
    Inject,
    Param,
    Patch,
    Body,
    Post,
    Get,
} from '@nestjs/common';

@ApiHeader({
    name: 'Authorization',
    description: 'Bearer access token',
    example: 'Bearer <accessToken>',
    required: true,
})
@ApiException(() => new UnauthorizedException('Invalid access token'), {
    description: 'Пользователь поставил некорректный токен доступа',
})
@ApiTags('Корзина')
@UseGuards(JwtAuthGuard)
@Controller('/cart/')
export class CartsController {
    constructor(
        @Inject(CartsService)
        private readonly cartsService: CartsService,
    ) {}

    @ApiOperation({ summary: 'Получение корзины текущего пользователя' })
    @ApiOkResponse({
        description: 'Пользователь получает объект корзины',
        type: CartDto,
    })
    @Get('/')
    public getCart(@User() client: UserEntity): Promise<CartEntity> {
        return this.cartsService.getCart(client);
    }

    @ApiOkResponse({
        description: 'Пользователь успешно удалил элемент из корзины',
        type: CartDto,
    })
    @ApiParam({
        name: 'itemId',
        description: 'Идентификатор предмета корзины',
        type: Number,
    })
    @ApiException(() => new NotFoundException('Cart item not found'), {
        description: 'Элемент корзины с указанным идентификатором не найден',
    })
    @ApiOperation({ summary: 'Удаление элемента из корзины пользователя' })
    @HttpCode(HttpStatus.OK)
    @Delete('/:itemId/')
    public removeItemFromCart(
        @User() client: UserEntity,
        @Param('itemId', ParseIntPipe) itemId: number,
    ): Promise<CartEntity> {
        return this.cartsService.removeItemFromCart(client, itemId);
    }

    @ApiBody({
        type: QuantityDto,
    })
    @ApiParam({
        name: 'itemId',
        description: 'Идентификатор предмета корзины',
        type: Number,
    })
    @ApiOkResponse({
        description:
            'Пользователь успешно обновил количество определенного предмета в корзине',
        type: CartDto,
    })
    @ApiException(() => new NotFoundException('Cart item not found'), {
        description: 'Элемент корзины с указанным идентификатором не найден',
    })
    @ApiOperation({
        summary: 'Изменение количества позиции в корзине пользователя',
    })
    @HttpCode(HttpStatus.OK)
    @Patch('/:itemId/')
    public setQuantityOfItem(
        @User() client: UserEntity,
        @Param('itemId', ParseIntPipe) itemId: number,
        @Body() data: QuantityDto,
    ): Promise<CartEntity> {
        return this.cartsService.setQuantityOfItem(
            client,
            itemId,
            data.quantity,
        );
    }

    @ApiParam({
        name: 'productId',
        description: 'Идентификатор продукта',
        type: Number,
    })
    @ApiCreatedResponse({
        description: 'Пользователь успешно добавил продукт в свою корзину',
    })
    @ApiException(() => new NotFoundException('Product not found'), {
        description: 'Продукт с указанным идентификатором не был найден',
    })
    @ApiOperation({ summary: 'Добавление продукта в корзину пользователя' })
    @HttpCode(HttpStatus.CREATED)
    @Post('/products/:productId/')
    public addProductToCart(
        @User() client: UserEntity,
        @Param('productId', ParseIntPipe) productId: number,
    ): Promise<void> {
        return this.cartsService.addProductToCart(client, productId);
    }

    @ApiParam({
        name: 'assemblyId',
        description: 'Идентификатор сборки',
        type: Number,
    })
    @ApiCreatedResponse({
        description:
            'Пользователь успешно добавил сборку из конфигуратора в свою корзину',
    })
    @ApiException(() => new NotFoundException('Assembly not found'), {
        description: 'Сборка с указанным идентификатором не была найдена',
    })
    @ApiOperation({ summary: 'Добавление сборки в корзину пользователя' })
    @HttpCode(HttpStatus.CREATED)
    @Post('/assemblies/:assemblyId/')
    public addAssemblyToCart(
        @User() client: UserEntity,
        @Param('assemblyId', ParseIntPipe) assemblyId: number,
    ): Promise<void> {
        return this.cartsService.addAssemblyToCart(client, assemblyId);
    }
}
