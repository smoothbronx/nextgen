import { Injectable, NotFoundException } from '@nestjs/common';
import { UserEntity } from '@/users/user.entity';
import { Repository } from 'typeorm';
import { CartEntity } from '@/carts/entities/cart.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CartItemEntity } from '@/carts/entities/item.entity';
import { AssemblyEntity } from '@/assemblies/entities/assembly.entity';
import { ProductEntity } from '@/products/product.entity';

@Injectable()
export class CartsService {
    constructor(
        @InjectRepository(CartEntity)
        private readonly cartsRepository: Repository<CartEntity>,
        @InjectRepository(CartItemEntity)
        private readonly cartItemsRepository: Repository<CartItemEntity>,
        @InjectRepository(AssemblyEntity)
        private readonly assembliesRepository: Repository<AssemblyEntity>,
        @InjectRepository(ProductEntity)
        private readonly productsRepository: Repository<ProductEntity>,
    ) {}

    public async getCart(client: UserEntity): Promise<CartEntity> {
        const cart = await this.cartsRepository.findOneBy({
            client: { id: client.id },
        });

        if (cart) {
            return cart;
        }

        return this.cartsRepository
            .create({
                client: { id: client.id },
            })
            .save();
    }

    public async removeItemFromCart(
        client: UserEntity,
        itemId: number,
    ): Promise<CartEntity> {
        const cart = await this.getCart(client);

        const item = await this.getItemOfCart(cart, itemId);

        await item.remove();
        await cart.reload();

        return cart;
    }

    public async setQuantityOfItem(
        client: UserEntity,
        itemId: number,
        quantity: number,
    ): Promise<CartEntity> {
        const cart = await this.getCart(client);
        const item = await this.getItemOfCart(cart, itemId);

        item.quantity = quantity;

        await item.save();
        await cart.reload();

        return cart;
    }

    public async addProductToCart(
        client: UserEntity,
        productId: number,
    ): Promise<void> {
        const productExists = await this.productsRepository.exist({
            where: { id: productId },
        });

        if (!productExists) {
            throw new NotFoundException('Product not found');
        }

        await this.addItemToCart(client, productId, false);
    }

    public async addAssemblyToCart(
        client: UserEntity,
        assemblyId: number,
    ): Promise<void> {
        const assembly = await this.assembliesRepository.findOne({
            where: { id: assemblyId },
        });

        if (!assembly) {
            throw new NotFoundException('Assembly not found');
        }

        await this.addItemToCart(client, assemblyId, true);
    }

    private async addItemToCart(
        client: UserEntity,
        itemId: number,
        isAssembly: boolean,
    ): Promise<void> {
        const cart = await this.getCart(client);

        await this.cartItemsRepository
            .create({
                ...(isAssembly
                    ? { assembly: { id: itemId } }
                    : { product: { id: itemId } }),
                cart: { id: cart.id },
                quantity: 1,
                isAssembly,
            })
            .save();
    }

    private async getItemOfCart(
        cart: CartEntity,
        itemId,
    ): Promise<CartItemEntity> {
        const item = await this.cartItemsRepository.findOneBy({
            cart: { id: cart.id },
            id: itemId,
        });

        if (!item) {
            throw new NotFoundException('Cart item not found');
        }

        return item;
    }
}
