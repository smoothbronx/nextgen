import { AssemblyDto } from '@/assemblies/assembly.dto';
import { ProductDto } from '@/products/dto/product.dto';
import { ApiProperty } from '@nestjs/swagger';

export class CartItemDto {
    @ApiProperty({
        name: 'id',
        description: 'Уникальный идентификатор элемента корзины',
        nullable: false,
        readOnly: true,
    })
    public id: number;

    @ApiProperty({
        name: 'product',
        description: 'Продукт, который пользователь покупает',
        type: ProductDto,
        nullable: true,
        readOnly: true,
    })
    public product: ProductDto | null;

    @ApiProperty({
        name: 'assembly',
        description: 'Сборка, которую пользователь покупает',
        type: AssemblyDto,
        nullable: true,
        readOnly: true,
    })
    public assembly: AssemblyDto | null;

    @ApiProperty({
        name: 'isAssembly',
        description: 'Флаг, показывающий чем является элемент корзины',
        nullable: false,
        readOnly: true,
    })
    public isAssembly: boolean;

    @ApiProperty({
        name: 'quantity',
        description: 'Количество конкретно этого товара',
        nullable: false,
        readOnly: true,
    })
    public quantity: number;

    @ApiProperty({
        name: 'price',
        description: 'Цена конкретно этого товара с учетом его количества',
        nullable: false,
        readOnly: true,
    })
    public price: number;
}
