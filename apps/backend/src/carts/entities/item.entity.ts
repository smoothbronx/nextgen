import { AssemblyEntity } from '@/assemblies/entities/assembly.entity';
import { ProductEntity } from '@/products/product.entity';
import { CartEntity } from '@/carts/entities/cart.entity';
import { Expose } from 'class-transformer';
import {
    PrimaryGeneratedColumn,
    BaseEntity,
    JoinColumn,
    ManyToOne,
    Column,
    Entity,
} from 'typeorm';

@Entity('cart_items')
export class CartItemEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @JoinColumn({ name: 'cart_id' })
    @ManyToOne(() => CartEntity, (cart) => cart.items, {
        nullable: false,
    })
    public cart: CartEntity;

    @JoinColumn({ name: 'product_id' })
    @ManyToOne(() => ProductEntity, {
        onDelete: 'CASCADE',
        nullable: true,
        eager: true,
    })
    public product: ProductEntity | null;

    @JoinColumn({ name: 'assembly_id' })
    @ManyToOne(() => AssemblyEntity, {
        onDelete: 'CASCADE',
        nullable: true,
        eager: true,
    })
    public assembly: AssemblyEntity | null;

    @Column({ name: 'is_assembly', type: 'boolean', nullable: false })
    public isAssembly: boolean;

    @Column({ name: 'quantity', nullable: false, default: 1 })
    public quantity: number;

    @Expose({ toPlainOnly: true })
    public price() {
        if (this.isAssembly && this.assembly) {
            return this.assembly.price() * this.quantity;
        }

        if (this.product) {
            return this.product.price() * this.quantity;
        }

        return 0;
    }
}
