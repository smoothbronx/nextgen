import { CartItemEntity } from '@/carts/entities/item.entity';
import { UserEntity } from '@/users/user.entity';
import { Expose } from 'class-transformer';
import {
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToMany,
    OneToOne,
    Entity,
    JoinColumn,
} from 'typeorm';

@Entity('carts')
export class CartEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @JoinColumn({ name: 'user_id' })
    @OneToOne(() => UserEntity, (client) => client.cart, {
        onDelete: 'CASCADE',
        nullable: false,
    })
    public client: UserEntity;

    @OneToMany(() => CartItemEntity, (item) => item.cart, {
        nullable: false,
        eager: true,
    })
    public items: CartItemEntity[];

    @Expose({ name: 'totalPrice', toPlainOnly: true })
    public getTotalPrice(): number {
        if (!this.items || this.items.length === 0) {
            return 0;
        }

        return this.items
            .map((product) => product.price())
            .reduce((prev, next) => prev + next);
    }
}
