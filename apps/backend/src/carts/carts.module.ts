import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CartEntity } from '@/carts/entities/cart.entity';
import { CartItemEntity } from '@/carts/entities/item.entity';
import { CartsController } from '@/carts/carts.controller';
import { CartsService } from '@/carts/carts.service';
import { AssembliesModule } from '@/assemblies/assemblies.module';
import { ProductsModule } from '@/products/products.module';

const cardsTypeormModule = TypeOrmModule.forFeature([
    CartItemEntity,
    CartEntity,
]);

@Module({
    imports: [cardsTypeormModule, AssembliesModule, ProductsModule],
    providers: [CartsService],
    controllers: [CartsController],
    exports: [cardsTypeormModule, CartsService],
})
export class CartsModule {}
