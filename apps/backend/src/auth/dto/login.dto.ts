import { ApiProperty } from '@nestjs/swagger';

export class LoginDto {
    @ApiProperty({
        name: 'login',
        example: 'smoothbronx',
        writeOnly: true,
        required: true,
    })
    public login: string;

    @ApiProperty({
        name: 'password',
        example: 'SomePassword1',
        writeOnly: true,
        required: true,
    })
    public password: string;
}
