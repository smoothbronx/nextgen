import { ApiProperty } from '@nestjs/swagger';
import {
    IsEmail,
    IsString,
    Matches,
    MaxLength,
    MinLength,
} from 'class-validator';

export class RegisterDto {
    @IsString()
    @ApiProperty({
        name: 'login',
        example: 'smoothbronx',
        writeOnly: true,
        required: true,
    })
    public login: string;

    @IsString()
    @ApiProperty({
        name: 'firstname',
        example: 'Владик',
        writeOnly: true,
        required: true,
    })
    public firstname: string;

    @IsString()
    @ApiProperty({
        name: 'lastname',
        example: 'Фамилия',
        writeOnly: true,
        required: true,
    })
    public lastname: string;

    @IsEmail()
    @ApiProperty({
        name: 'email',
        example: 'smoothbronx@xenofium.com',
        writeOnly: true,
        required: true,
    })
    public email: string;

    @IsString()
    @MinLength(6)
    @MaxLength(20)
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
        message: 'Этот пароль слишком простой',
    })
    @ApiProperty({
        name: 'password',
        example: 'SomePassword1',
        writeOnly: true,
        required: true,
    })
    public password: string;

    @IsString()
    @ApiProperty({
        name: 'passwordRepeated',
        example: 'SomePassword1',
        writeOnly: true,
        required: true,
    })
    public passwordRepeated: string;
}
