import { ApiException } from '@nanogiants/nestjs-swagger-api-exception-decorator';
import { AuthTokens, AuthTokensDto } from 'src/@types/Tokens';
import { Token } from 'src/@shared/handlers/token.handler';
import { RegisterDto } from 'src/auth/dto/register.dto';
import { AuthService } from 'src/auth/auth.service';
import { LoginDto } from 'src/auth/dto/login.dto';
import {
    UnauthorizedException,
    BadRequestException,
    HttpStatus,
    Controller,
    HttpCode,
    Inject,
    Body,
    Post,
    Get,
} from '@nestjs/common';
import {
    ApiCreatedResponse,
    ApiOkResponse,
    ApiOperation,
    ApiHeader,
    ApiBody,
    ApiTags,
} from '@nestjs/swagger';

@ApiTags('Аутентификация')
@Controller('/auth/')
export class AuthController {
    constructor(
        @Inject(AuthService)
        private readonly authService: AuthService,
    ) {}

    @ApiBody({
        type: RegisterDto,
        description: 'Реквизиты пользователя для регистрации',
    })
    @ApiCreatedResponse({
        description:
            'Пользователь успешно зарегистрировался в системе. Приложения ждет подтверждения почты.',
    })
    @ApiException(() => new BadRequestException('Passwords does not match'), {
        description: 'Введенные пароли не совпадают',
    })
    @ApiException(
        () => new BadRequestException('User with this login exists'),
        {
            description: 'Пользователь с таким логином уже существует',
        },
    )
    @ApiException(
        () => new BadRequestException('User with this email exists'),
        {
            description:
                'Пользователь с таким адресом электронной почты уже существует',
        },
    )
    @ApiOperation({ summary: 'Регистрация пользователя в системе' })
    @HttpCode(HttpStatus.CREATED)
    @Post('/register/')
    public register(@Body() registerCredentials: RegisterDto): Promise<void> {
        return this.authService.register(registerCredentials);
    }

    @ApiBody({
        type: LoginDto,
        description: 'Реквизиты пользователя для авторизации',
    })
    @ApiException(() => new UnauthorizedException('Incorrect password'), {
        description: 'Неправильный пароль пользователя',
    })
    @ApiException(
        () => new UnauthorizedException('Account must be verified by email'),
        {
            description:
                'Аккаунт пользователя прежде чем быть авторизованным, должен быть верифицирован по почте',
        },
    )
    @ApiException(() => new UnauthorizedException('Incorrect login'), {
        description: 'Неправильно введен логин пользователя',
    })
    @ApiOkResponse({
        description:
            'Пользователь успешно авторизовался в системе и получил пару токенов',
        type: AuthTokensDto,
    })
    @ApiOperation({ summary: 'Авторизация пользователя в системе' })
    @HttpCode(HttpStatus.OK)
    @Post('/login/')
    public login(@Body() loginCredentials: LoginDto): Promise<AuthTokens> {
        return this.authService.login(loginCredentials);
    }

    @ApiHeader({
        name: 'Authorization',
        description: 'Bearer refresh token',
        example: 'Bearer <refreshToken>',
        required: true,
    })
    @ApiException(() => new UnauthorizedException('Invalid refresh token'), {
        description:
            'Пользователь поставил некорректный токен обновления сессии',
    })
    @ApiOkResponse({
        description:
            'Пользователь успешно продлил свою сессию и получил новую пару токенов',
        type: AuthTokensDto,
    })
    @ApiOperation({ summary: 'Продление сессии пользователя' })
    @HttpCode(HttpStatus.OK)
    @Get('/refresh/')
    public async refreshTokens(@Token() refreshToken: string) {
        return this.authService.refreshTokens(refreshToken);
    }
}
