import { VerificationsService } from 'src/verifications/verifications.service';
import { TokenPayload, AuthTokens } from 'src/@types/Tokens';
import { CryptoProvider } from 'src/@shared/crypto.provider';
import { UsersService } from 'src/users/users.service';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from 'src/users/user.entity';
import { RegisterDto } from 'src/auth/dto/register.dto';
import { ConfigService } from '@nestjs/config';
import { LoginDto } from 'src/auth/dto/login.dto';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import {
    UnauthorizedException,
    BadRequestException,
    Injectable,
    Inject,
} from '@nestjs/common';

@Injectable()
export class AuthService {
    private readonly cryptoProvider = CryptoProvider;

    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        @Inject(JwtService)
        private readonly jwtService: JwtService,
        @Inject(UsersService)
        private readonly usersService: UsersService,
        @Inject(ConfigService)
        private readonly configService: ConfigService,
        @Inject(VerificationsService)
        private readonly verificationsService: VerificationsService,
    ) {}

    public async register(userCredentials: RegisterDto): Promise<void> {
        if (userCredentials.password !== userCredentials.passwordRepeated) {
            throw new BadRequestException('Passwords does not match');
        }

        userCredentials.password = this.cryptoProvider.generateHashFromPassword(
            userCredentials.password,
        );

        await this.validateRegisterCredentials(userCredentials);
        await this.usersService.createUser(userCredentials);
        await this.verificationsService.createVerification(
            userCredentials.email,
        );
    }

    private async validateRegisterCredentials(credentials: RegisterDto) {
        if (await this.usersService.userExistsByEmail(credentials.email)) {
            throw new BadRequestException('User with this email exists');
        }

        if (await this.usersService.getUserByLogin(credentials.login)) {
            throw new BadRequestException('User with this login exists');
        }
    }

    public async refreshTokens(refreshToken: string): Promise<AuthTokens> {
        try {
            const token = this.jwtService.verify<TokenPayload>(refreshToken, {
                publicKey: this.configService.getOrThrow('JWT_PRIVATE_KEY'),
            });

            const user = await this.usersService.getUserByLoginAndRefresh(
                token.login,
                refreshToken,
            );

            return await this.generateNewTokensPair(user);
        } catch {
            throw new UnauthorizedException('Invalid refresh token');
        }
    }

    public async login(userCredentials: LoginDto): Promise<AuthTokens> {
        const user = await this.usersService.getUserByLogin(
            userCredentials.login,
        );

        if (!user) {
            throw new UnauthorizedException('Incorrect login');
        }

        await this.validateUserCredentialsOnLogin(user, userCredentials);
        return this.generateNewTokensPair(user);
    }

    private async validateUserCredentialsOnLogin(
        targetUser: UserEntity,
        userCredentials: LoginDto,
    ) {
        if (!targetUser.isActive) {
            throw new UnauthorizedException(
                'Account must be verified by email',
            );
        }

        const isMatch: boolean = this.cryptoProvider.passwordMatch(
            userCredentials.password,
            targetUser.password,
        );

        if (!isMatch) {
            throw new UnauthorizedException('Incorrect password');
        }
    }

    private async generateNewTokensPair(user: UserEntity): Promise<AuthTokens> {
        const tokens: AuthTokens = this.generateNewAuthTokensPair(user);
        await this.updateUserRefreshToken(user, tokens.refreshToken);
        return tokens;
    }

    private generateNewAuthTokensPair(user: UserEntity): AuthTokens {
        const getTokenPayload = (type): Partial<TokenPayload> => ({
            login: user.login,
            role: user.role,
            type: type,
        });

        return {
            accessToken: this.jwtService.sign(
                {
                    ...getTokenPayload('access'),
                },
                {
                    privateKey: process.env.JWT_PRIVATE_KEY,
                    expiresIn: '30m',
                    algorithm: 'HS512',
                    issuer: 'nextgen',
                },
            ),
            refreshToken: this.jwtService.sign(
                {
                    ...getTokenPayload('refresh'),
                },
                {
                    privateKey: process.env.JWT_PRIVATE_KEY,
                    expiresIn: '12h',
                    algorithm: 'HS512',
                    issuer: 'nextgen',
                },
            ),
        };
    }

    private updateUserRefreshToken(user: UserEntity, refreshToken: string) {
        user.refresh = refreshToken;
        return user.save();
    }
}
