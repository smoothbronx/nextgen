import { VerificationsModule } from '@/verifications/verifications.module';
import { AuthController } from '@/auth/auth.controller';
import { UsersService } from '@/users/users.service';
import { UsersModule } from '@/users/users.module';
import { AuthService } from '@/auth/auth.service';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { Module } from '@nestjs/common';
import { CartsModule } from '@/carts/carts.module';
import { ConfigModule, ConfigType } from '@nestjs/config';
import { JwtConfigWorkspace } from '@/config/workspaces/jwt.workspace';

@Module({
    imports: [
        JwtModule.registerAsync({
            imports: [ConfigModule.forFeature(JwtConfigWorkspace)],
            inject: [JwtConfigWorkspace.KEY],
            useFactory: (jwtConfig: ConfigType<typeof JwtConfigWorkspace>) => ({
                global: true,
                privateKey: jwtConfig.keys.private,
                publicKey: jwtConfig.keys.public,
            }),
        }),
        UsersModule,
        VerificationsModule,
        CartsModule,
    ],
    providers: [AuthService, JwtService, UsersService],
    controllers: [AuthController],
    exports: [AuthService],
})
export class AuthModule {}
