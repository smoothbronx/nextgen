import { DocumentBuilder } from '@nestjs/swagger';

export const swaggerConfig = new DocumentBuilder()
    .setTitle('Nextgen')
    .setVersion('1.0')
    .addTag('Аутентификация')
    .addTag('Верификация')
    .build();
