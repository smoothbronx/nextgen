import { DatabaseConfigWorkspace } from '@/config/workspaces/database.workspace';
import { getConfigurationModule } from '@/config/config.module';
import { VerificationsModule } from '@/verifications/verifications.module';
import { VerificationEntity } from '@/verifications/verification.entity';
import { ConfigModule, ConfigType } from '@nestjs/config';
import { UsersModule } from '@/users/users.module';
import { MailsModule } from '@/mails/mails.module';
import { UserEntity } from '@/users/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '@/auth/auth.module';
import { Module } from '@nestjs/common';
import { MotherboardEntity } from '@/components/submodules/motherboards/motherboard.entity';
import { PowerSupplyEntity } from '@/components/submodules/powerSupplies/powerSupply.entity';
import { HardDriveEntity } from '@/components/submodules/hardDrives/hardDrive.entity';
import { ProcessorEntity } from '@/components/submodules/processors/processor.entity';
import { RamMemoryEntity } from '@/components/submodules/ramMemory/ramMemory.entity';
import { VideoCardEntity } from '@/components/submodules/videoCards/videoCard.entity';
import { ComponentsModule } from '@/components/components.module';
import { CartsModule } from '@/carts/carts.module';
import { JwtStrategy } from 'shared/jwt/jwt.strategy';
import { AssembliesModule } from '@/assemblies/assemblies.module';
import { ProductsModule } from '@/products/products.module';
import { StaticModule } from '@/staticfiles/staticfiles.module';
import { OrderEntity } from '@/orders/entities/order.entity';
import { OrderItemEntity } from '@/orders/entities/item.entity';
import { CartEntity } from '@/carts/entities/cart.entity';
import { CartItemEntity } from '@/carts/entities/item.entity';
import { OrdersModule } from '@/orders/orders.module';
import { AssemblyRamEntity } from '@/assemblies/entities/ram.entity';
import { AssemblyHardDriveEntity } from '@/assemblies/entities/hards.entity';

@Module({
    imports: [
        getConfigurationModule(),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule.forFeature(DatabaseConfigWorkspace)],
            inject: [DatabaseConfigWorkspace.KEY],
            useFactory: (
                databaseConfig: ConfigType<typeof DatabaseConfigWorkspace>,
            ) => ({
                type: 'postgres',
                ...databaseConfig,
                extra: {
                    ssl: false,
                },
                ssl: {
                    rejectUnauthorized: false,
                },
                entities: [
                    UserEntity,
                    OrderEntity,
                    OrderItemEntity,
                    CartEntity,
                    CartItemEntity,
                    AssemblyHardDriveEntity,
                    VerificationEntity,
                    MotherboardEntity,
                    PowerSupplyEntity,
                    AssemblyRamEntity,
                    HardDriveEntity,
                    ProcessorEntity,
                    RamMemoryEntity,
                    VideoCardEntity,
                ],
                autoLoadEntities: true,
                synchronize: true,
            }),
        }),
        VerificationsModule,
        AssembliesModule,
        ComponentsModule,
        ProductsModule,
        StaticModule,
        OrdersModule,
        CartsModule,
        MailsModule,
        UsersModule,
        AuthModule,
    ],
    controllers: [],
    providers: [JwtStrategy],
})
export class AppModule {}
