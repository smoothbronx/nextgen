import { ApiProperty } from '@nestjs/swagger';
import { Roles } from 'src/@types/Roles';

export class UserDto {
    public id: number;

    public firstname?: string;

    public lastname?: string;

    public email?: string;

    public address?: string;

    public phone: string;

    public login: string;

    @ApiProperty({
        name: 'role',
        readOnly: true,
        enum: Roles,
        example: Roles.CLIENT,
    })
    public role: Roles;
}
