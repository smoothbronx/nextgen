import { RegisterDto } from '@/auth/dto/register.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '@/users/user.entity';
import { Repository } from 'typeorm';
import {
    ConflictException,
    Inject,
    Injectable,
    NotFoundException,
} from '@nestjs/common';
import { Roles } from 'types/Roles';
import { CartsService } from '@/carts/carts.service';
import { CryptoProvider } from 'shared/crypto.provider';

@Injectable()
export class UsersService {
    private readonly cryptoProvider = CryptoProvider;

    constructor(
        @InjectRepository(UserEntity)
        private readonly usersRepository: Repository<UserEntity>,
        @Inject(CartsService)
        private readonly cartsService: CartsService,
    ) {}

    public async createAdmin(): Promise<any> {
        const adminExists = await this.usersRepository.exist({
            where: { login: 'admin' },
        });
        if (!adminExists) {
            return this.usersRepository
                .create({
                    login: 'admin',
                    email: 'admin@road39.ru',
                    firstname: 'Администратор',
                    lastname: 'Василий',
                    password:
                        this.cryptoProvider.generateHashFromPassword('root'),
                    role: Roles.ADMIN,
                    isActive: true,
                })
                .save();
        }
    }

    public async createUser(credentials: RegisterDto): Promise<UserEntity> {
        let userExists = false;

        if (await this.userExistsByEmail(credentials.email)) {
            userExists = true;
        }

        if (await this.userExistsByLogin(credentials.login)) {
            userExists = true;
        }

        if (userExists) {
            throw new ConflictException('User exists');
        }

        const user = await this.usersRepository
            .create({
                login: credentials.login,
                email: credentials.email,
                firstname: credentials.firstname,
                lastname: credentials.lastname,
                password: credentials.password,
                role: Roles.CLIENT,
            })
            .save();
        await this.cartsService.getCart(user);
        return user.save();
    }

    public async userExistsByLogin(login: string): Promise<boolean> {
        return Boolean(await this.usersRepository.findOneBy({ login }));
    }

    public async userExistsByEmail(email: string): Promise<boolean> {
        return Boolean(await this.usersRepository.findOneBy({ email }));
    }

    public getUserByLogin(login: string): Promise<UserEntity | null> {
        return this.usersRepository.findOneBy({ login });
    }

    public getUserByEmail(email: string): Promise<UserEntity | null> {
        return this.usersRepository.findOneBy({ email });
    }

    public async getUserByLoginAndRefresh(
        login: string,
        refresh: string,
    ): Promise<UserEntity> {
        const user = await this.usersRepository.findOneBy({ login, refresh });

        if (!user) {
            throw new NotFoundException('User does not exists');
        }

        return user;
    }
}
