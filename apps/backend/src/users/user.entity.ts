import { CartEntity } from '@/carts/entities/cart.entity';
import { Roles } from 'src/@types/Roles';
import {
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToOne,
    Column,
    Entity,
    JoinColumn,
    OneToMany,
} from 'typeorm';
import { OrderEntity } from '@/orders/entities/order.entity';

@Entity('users')
export class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ name: 'firstname', type: 'varchar', nullable: true })
    public firstname: string;

    @Column({ name: 'lastname', type: 'varchar', nullable: true })
    public lastname: string;

    @Column({ name: 'email', type: 'varchar', nullable: true, unique: true })
    public email: string;

    @Column({ name: 'address', type: 'varchar', nullable: true })
    public address: string;

    @Column({ name: 'phone', type: 'varchar', nullable: true })
    public phone: string;

    @Column({ name: 'login', type: 'varchar', nullable: false, unique: true })
    public login: string;

    @Column({ name: 'password', type: 'varchar', nullable: false })
    public password: string;

    @Column({ name: 'role', enum: Roles })
    public role: Roles;

    @Column({ name: 'refresh', type: 'varchar', nullable: true })
    public refresh: string | null;

    @JoinColumn({ name: 'cart_id' })
    @OneToOne(() => CartEntity, (cart) => cart.client, {
        nullable: true,
        cascade: true,
        eager: true,
    })
    public cart: CartEntity | null;

    @OneToMany(() => OrderEntity, (order) => order.client, {
        cascade: true,
        eager: true,
    })
    public orders: OrderEntity[];

    @Column({
        name: 'active',
        type: 'boolean',
        nullable: false,
        default: false,
    })
    public isActive: boolean;
}
