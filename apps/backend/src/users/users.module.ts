import { VerificationsModule } from '@/verifications/verifications.module';
import { UsersService } from '@/users/users.service';
import { forwardRef, Inject, Module } from '@nestjs/common';
import { CartsModule } from '@/carts/carts.module';
import { UserEntity } from '@/users/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

const dynamicTypeormModule = TypeOrmModule.forFeature([UserEntity]);

@Module({
    imports: [
        dynamicTypeormModule,
        forwardRef(() => VerificationsModule),
        forwardRef(() => CartsModule),
    ],
    providers: [UsersService],
    exports: [dynamicTypeormModule, UsersService],
})
export class UsersModule {
    constructor(@Inject(UsersService) userService: UsersService) {
        userService.createAdmin().then();
    }
}
