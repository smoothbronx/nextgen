import { Module } from '@nestjs/common';
import { StaticService } from '@/staticfiles/staticfiles.service';
import { StaticController } from '@/staticfiles/staticfiles.controller';
import { CryptoProvider } from 'shared/crypto.provider';

@Module({
    imports: [],
    providers: [StaticService, CryptoProvider],
    controllers: [StaticController],
    exports: [StaticService],
})
export class StaticModule {}
