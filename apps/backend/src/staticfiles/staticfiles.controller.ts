import { Controller, Inject, Param, Get, Res } from '@nestjs/common';
import { StaticService } from '@/staticfiles/staticfiles.service';
import { Response } from 'express';

@Controller('/static/')
export class StaticController {
    constructor(
        @Inject(StaticService)
        private readonly staticService: StaticService,
    ) {}

    @Get('/:filename/')
    public getFile(
        @Res() response: Response,
        @Param('filename') filename: string,
    ) {
        this.staticService.getFile(response, filename);
    }
}
