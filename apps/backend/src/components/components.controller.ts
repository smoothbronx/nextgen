import { ApiException } from '@nanogiants/nestjs-swagger-api-exception-decorator';
import { ComponentsDto, ComponentsTypesDto } from '@/components/components.dto';
import { ComponentsService } from '@/components/components.service';
import { JwtAuthGuard } from 'shared/jwt/jwt.guard';
import {
    UnauthorizedException,
    BadRequestException,
    Controller,
    HttpStatus,
    UseGuards,
    HttpCode,
    Inject,
    Body,
    Post,
} from '@nestjs/common';
import {
    ApiHeader,
    ApiOkResponse,
    ApiOperation,
    ApiTags,
} from '@nestjs/swagger';

@ApiHeader({
    name: 'Authorization',
    description: 'Bearer access token',
    example: 'Bearer <accessToken>',
    required: true,
})
@ApiException(() => new UnauthorizedException('Invalid access token'), {
    description: 'Пользователь поставил некорректный токен доступа',
})
@ApiTags('Компоненты')
@UseGuards(JwtAuthGuard)
@Controller('/components/')
export class ComponentsController {
    constructor(
        @Inject(ComponentsService)
        private readonly componentsService: ComponentsService,
    ) {}

    @ApiOkResponse({
        description: 'Пользователь успешно получил компоненты по их типам',
        type: ComponentsDto,
    })
    @ApiException(() => new BadRequestException('Incorrect component type'), {
        description: 'Указан неправильный тип компонента',
    })
    @ApiOperation({ summary: 'Получение всех компонентов по типам' })
    @HttpCode(HttpStatus.OK)
    @Post('/')
    public getAllComponents(
        @Body() components: ComponentsTypesDto,
    ): Promise<ComponentsDto> {
        return this.componentsService.getComponents(components);
    }
}
