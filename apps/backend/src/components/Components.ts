export enum Components {
    HARD_DRIVE = 'hards',
    MOTHERBOARD = 'motherboards',
    POWER_SUPPLY = 'power',
    PROCESSOR = 'processors',
    RAM_MEMORY = 'ram',
    VIDEO_CARD = 'video',
}
