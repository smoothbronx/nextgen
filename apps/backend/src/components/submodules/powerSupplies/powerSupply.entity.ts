import { ComponentEntityBase } from '@/components/submodules/base/component.entity.base';
import { Column, Entity } from 'typeorm';

@Entity('power_supplies')
export class PowerSupplyEntity extends ComponentEntityBase {
    @Column({ name: 'max_power', nullable: false })
    public maxPower: number;

    @Column({ name: 'certificate', type: 'varchar', nullable: false })
    public certificate: string;
}
