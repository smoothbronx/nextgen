import { ComponentDtoBase } from '@/components/submodules/base/component.dto.base';
import { IsInt, IsPositive, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PowerSupplyDto extends ComponentDtoBase {
    @IsInt()
    @IsPositive()
    @ApiProperty({
        name: 'maxPower',
        description: 'Максимальная мощность',
        required: true,
    })
    public maxPower: number;

    @IsString()
    @ApiProperty({
        name: 'certificate',
        description: 'Сертификация блока питания',
        required: true,
    })
    public certificate: string;
}
