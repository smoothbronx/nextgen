import { ComponentsServiceBase } from '@/components/submodules/base/components.service.base';
import { PowerSupplyEntity } from '@/components/submodules/powerSupplies/powerSupply.entity';
import { PowerSupplyDto } from '@/components/submodules/powerSupplies/powerSupply.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

export class PowerSuppliesService extends ComponentsServiceBase<
    PowerSupplyEntity,
    PowerSupplyDto
> {
    protected readonly componentName = 'Power supply';

    constructor(
        @InjectRepository(PowerSupplyEntity)
        protected readonly repository: Repository<PowerSupplyEntity>,
    ) {
        super();
    }
}
