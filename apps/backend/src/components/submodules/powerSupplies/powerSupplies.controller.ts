import { ComponentsControllerBase } from '@/components/submodules/base/components.controller.base';
import { PowerSuppliesService } from '@/components/submodules/powerSupplies/powerSupplies.service';
import { PowerSupplyEntity } from '@/components/submodules/powerSupplies/powerSupply.entity';
import { PowerSupplyDto } from '@/components/submodules/powerSupplies/powerSupply.dto';
import { Controller, Inject } from '@nestjs/common';

@Controller('/components/power/')
export class PowerSuppliesController extends ComponentsControllerBase<
    PowerSupplyEntity,
    PowerSupplyDto
> {
    constructor(
        @Inject(PowerSuppliesService)
        protected readonly service: PowerSuppliesService,
    ) {
        super();
    }
}
