import { ComponentEntityBase } from '@/components/submodules/base/component.entity.base';
import { Column, Entity } from 'typeorm';

@Entity('processors')
export class ProcessorEntity extends ComponentEntityBase {
    @Column({ name: 'core_number', nullable: false })
    public coreNumber: number;

    @Column({ name: 'clock_speed', nullable: false, type: 'float' })
    public clockSpeed: number;

    @Column({ name: 'socket', nullable: false })
    public socket: string;
}
