import { ComponentDtoBase } from '@/components/submodules/base/component.dto.base';
import { IsInt, IsPositive } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ProcessorDto extends ComponentDtoBase {
    @IsPositive()
    @IsInt()
    @ApiProperty({
        name: 'coreNumber',
        description: 'Число ядер процессора',
        required: true,
    })
    public coreNumber: number;

    @IsPositive()
    @IsInt()
    @ApiProperty({
        name: 'clockSpeed',
        description: 'Тактовая частота процессора',
        required: true,
    })
    public clockSpeed: number;

    @IsPositive()
    @IsInt()
    @ApiProperty({
        name: 'socket',
        description: 'Сокет Процессора',
        required: true,
    })
    public socket: string;
}
