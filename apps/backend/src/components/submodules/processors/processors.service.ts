import { ComponentsServiceBase } from '@/components/submodules/base/components.service.base';
import { ProcessorEntity } from '@/components/submodules/processors/processor.entity';
import { ProcessorDto } from '@/components/submodules/processors/processor.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class ProcessorsService extends ComponentsServiceBase<
    ProcessorEntity,
    ProcessorDto
> {
    protected componentName = 'Processor';

    constructor(
        @InjectRepository(ProcessorEntity)
        protected readonly repository: Repository<ProcessorEntity>,
    ) {
        super();
    }
}
