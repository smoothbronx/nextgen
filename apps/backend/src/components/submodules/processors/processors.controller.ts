import { ComponentsControllerBase } from '@/components/submodules/base/components.controller.base';
import { ProcessorsService } from '@/components/submodules/processors/processors.service';
import { ProcessorEntity } from '@/components/submodules/processors/processor.entity';
import { ProcessorDto } from '@/components/submodules/processors/processor.dto';
import { Controller, Inject } from '@nestjs/common';

@Controller('/components/processors/')
export class ProcessorsController extends ComponentsControllerBase<
    ProcessorEntity,
    ProcessorDto
> {
    constructor(
        @Inject(ProcessorsService)
        protected readonly service: ProcessorsService,
    ) {
        super();
    }
}
