import { ComponentEntityBase } from '@/components/submodules/base/component.entity.base';
import { Column, Entity } from 'typeorm';

@Entity('ram_memory')
export class RamMemoryEntity extends ComponentEntityBase {
    @Column({ name: 'type', type: 'varchar', nullable: false })
    public type: string;

    @Column({ name: 'speed', nullable: false })
    public speed: number;

    @Column({ name: 'capacity', nullable: false })
    public capacity: number;
}
