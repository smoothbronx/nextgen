import { ComponentsControllerBase } from '@/components/submodules/base/components.controller.base';
import { RamMemoriesService } from '@/components/submodules/ramMemory/ramMemories.service';
import { RamMemoryEntity } from '@/components/submodules/ramMemory/ramMemory.entity';
import { RamMemoryDto } from '@/components/submodules/ramMemory/ramMemory.dto';
import { Controller, Inject } from '@nestjs/common';

@Controller('/components/ram/')
export class RamMemoriesController extends ComponentsControllerBase<
    RamMemoryEntity,
    RamMemoryDto
> {
    constructor(
        @Inject(RamMemoriesService)
        protected readonly service: RamMemoriesService,
    ) {
        super();
    }
}
