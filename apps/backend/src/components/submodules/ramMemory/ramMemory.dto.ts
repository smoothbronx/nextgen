import { ComponentDtoBase } from '@/components/submodules/base/component.dto.base';
import { IsInt, IsPositive, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class RamMemoryDto extends ComponentDtoBase {
    @IsString()
    @ApiProperty({
        name: 'type',
        description: 'Тип оперативной памяти',
        required: true,
    })
    public type: string;

    @IsInt()
    @IsPositive()
    @ApiProperty({
        name: 'speed',
        description: 'Частота оперативной памяти',
        required: true,
    })
    public speed: number;

    @IsInt()
    @IsPositive()
    @ApiProperty({
        name: 'capacity',
        description: 'Объем оперативной памяти',
        required: true,
    })
    public capacity: number;
}
