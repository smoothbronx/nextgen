import { Injectable } from '@nestjs/common';
import { ComponentsServiceBase } from '@/components/submodules/base/components.service.base';
import { RamMemoryEntity } from '@/components/submodules/ramMemory/ramMemory.entity';
import { RamMemoryDto } from '@/components/submodules/ramMemory/ramMemory.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RamMemoriesService extends ComponentsServiceBase<
    RamMemoryEntity,
    RamMemoryDto
> {
    protected readonly componentName = 'Ram memory';

    constructor(
        @InjectRepository(RamMemoryEntity)
        protected readonly repository: Repository<RamMemoryEntity>,
    ) {
        super();
    }
}
