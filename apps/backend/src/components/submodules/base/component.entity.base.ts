import { BaseEntity, Column, PrimaryGeneratedColumn } from 'typeorm';

export class ComponentEntityBase extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ name: 'name', type: 'varchar', nullable: false })
    public name: string;

    @Column({ name: 'manufacturer', nullable: false, type: 'varchar' })
    public manufacturer: string;

    @Column({
        name: 'price',
        nullable: false,
        type: 'numeric',
        transformer: {
            to: (value) => value,
            from: (value) => parseFloat(value),
        },
    })
    public price: number;
}
