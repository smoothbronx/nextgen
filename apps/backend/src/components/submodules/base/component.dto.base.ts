import { IsDecimal, IsPositive, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ComponentDtoBase {
    @ApiProperty({
        name: 'id',
        readOnly: true,
        description: 'Идентификатор объекта компонента',
    })
    public id: number;

    @IsString()
    @ApiProperty({
        name: 'name',
        description: 'Название компонента',
        required: true,
    })
    public name: string;

    @IsString()
    @ApiProperty({
        name: 'manufacturer',
        description: 'Производитель компонента',
        required: true,
    })
    public manufacturer: string;

    @IsPositive()
    @IsDecimal()
    @ApiProperty({
        name: 'price',
        description: 'Цена компонента',
        required: true,
    })
    public price: number;
}
