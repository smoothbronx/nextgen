import { ComponentEntityBase } from '@/components/submodules/base/component.entity.base';
import { ComponentDtoBase } from '@/components/submodules/base/component.dto.base';
import { NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';

export class ComponentsServiceBase<
    EntityType extends ComponentEntityBase,
    DtoType extends ComponentDtoBase,
> {
    protected readonly componentName: string;
    protected readonly repository: Repository<any>;

    public async createComponent(componentDto: DtoType): Promise<void> {
        await this.repository.create(componentDto).save();
    }

    public getComponents(): Promise<EntityType[]> {
        return this.repository.find();
    }

    public async getComponent(componentId: number): Promise<EntityType> {
        const component = await this.repository.findOneBy({
            id: componentId,
        });

        if (!component) {
            throw new NotFoundException(`${this.componentName} not found`);
        }

        return component;
    }

    public async updateComponent(
        componentId: number,
        componentDto: DtoType,
    ): Promise<void> {
        await this.getComponent(componentId);
        await this.repository.update({ id: componentId }, componentDto);
    }

    public async deleteComponent(componentId: number): Promise<void> {
        const component = await this.getComponent(componentId);
        await component.remove();
    }
}
