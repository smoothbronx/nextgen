import { ComponentsServiceBase } from '@/components/submodules/base/components.service.base';
import { ComponentEntityBase } from '@/components/submodules/base/component.entity.base';
import { ComponentDtoBase } from '@/components/submodules/base/component.dto.base';
import { ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import {
    ParseIntPipe,
    HttpStatus,
    HttpCode,
    Delete,
    Patch,
    Param,
    Body,
    Post,
    Get,
    UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'shared/jwt/jwt.guard';

@ApiTags('Компоненты')
@UseGuards(JwtAuthGuard)
export class ComponentsControllerBase<
    EntityType extends ComponentEntityBase,
    DtoType extends ComponentDtoBase,
> {
    protected readonly service: ComponentsServiceBase<EntityType, DtoType>;

    @ApiOperation({
        summary: 'Получение всех компонентов',
    })
    @HttpCode(HttpStatus.OK)
    @Get('/')
    public getComponents(): Promise<EntityType[]> {
        return this.service.getComponents();
    }

    @ApiOperation({
        summary: 'Получение определенного компонента',
    })
    @HttpCode(HttpStatus.OK)
    @Get('/:componentId/')
    public getComponent(
        @Param('componentId', ParseIntPipe) componentId: number,
    ): Promise<EntityType> {
        return this.service.getComponent(componentId);
    }

    @ApiOperation({
        summary: 'Создание компонента',
    })
    @HttpCode(HttpStatus.CREATED)
    @Post('/')
    public createComponent(@Body() componentDto: DtoType): Promise<void> {
        return this.service.createComponent(componentDto);
    }

    @ApiOperation({
        summary: 'Изменение компонента',
    })
    @ApiParam({
        name: 'componentId',
        type: Number,
    })
    @HttpCode(HttpStatus.NO_CONTENT)
    @Patch('/:componentId/')
    public updateComponent(
        @Param('componentId', ParseIntPipe) componentId: number,
        @Body() componentDto: DtoType,
    ): Promise<void> {
        return this.service.updateComponent(componentId, componentDto);
    }

    @ApiOperation({
        summary: 'Удаление компонента',
    })
    @ApiParam({
        name: 'componentId',
        type: Number,
    })
    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete('/:componentId/')
    public deleteComponent(
        @Param('componentId', ParseIntPipe) componentId: number,
    ): Promise<void> {
        return this.service.deleteComponent(componentId);
    }
}
