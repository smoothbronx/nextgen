import { ComponentEntityBase } from '@/components/submodules/base/component.entity.base';
import { Column, Entity } from 'typeorm';

@Entity('motherboards')
export class MotherboardEntity extends ComponentEntityBase {
    @Column({ name: 'socket', type: 'varchar', nullable: false })
    public socket: string;

    @Column({ name: 'ram_quantity', nullable: false })
    public ramQuantity: number;

    @Column({ name: 'max_memory_capacity', nullable: false })
    public maxMemoryCapacity: number;
}
