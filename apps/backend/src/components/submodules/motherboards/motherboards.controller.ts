import { ComponentsControllerBase } from '@/components/submodules/base/components.controller.base';
import { MotherboardsService } from '@/components/submodules/motherboards/motherboards.service';
import { MotherboardEntity } from '@/components/submodules/motherboards/motherboard.entity';
import { MotherboardDto } from '@/components/submodules/motherboards/motherboard.dto';
import { Controller, Inject } from '@nestjs/common';

@Controller('/components/motherboards/')
export class MotherboardsController extends ComponentsControllerBase<
    MotherboardEntity,
    MotherboardDto
> {
    constructor(
        @Inject(MotherboardsService)
        protected readonly service: MotherboardsService,
    ) {
        super();
    }
}
