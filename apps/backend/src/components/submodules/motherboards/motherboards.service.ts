import { ComponentsServiceBase } from '@/components/submodules/base/components.service.base';
import { MotherboardEntity } from '@/components/submodules/motherboards/motherboard.entity';
import { MotherboardDto } from '@/components/submodules/motherboards/motherboard.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class MotherboardsService extends ComponentsServiceBase<
    MotherboardEntity,
    MotherboardDto
> {
    protected readonly componentName = 'Motherboard';

    constructor(
        @InjectRepository(MotherboardEntity)
        protected readonly repository: Repository<MotherboardEntity>,
    ) {
        super();
    }
}
