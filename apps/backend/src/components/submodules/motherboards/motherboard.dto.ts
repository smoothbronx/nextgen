import { ComponentDtoBase } from '@/components/submodules/base/component.dto.base';
import { IsInt, IsPositive, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class MotherboardDto extends ComponentDtoBase {
    @IsString()
    @ApiProperty({
        name: 'socket',
        description: 'Сокет материнской платы',
        required: true,
    })
    public socket: string;

    @IsPositive()
    @IsInt()
    @ApiProperty({
        name: 'ramQuantity',
        description: 'Количество мест под оперативную память',
        required: true,
    })
    public ramQuantity: number;

    @IsPositive()
    @IsInt()
    @ApiProperty({
        name: 'maxMemoryCapacity',
        description: 'Максимальный объем оперативной памяти',
        required: true,
    })
    public maxMemoryCapacity: number;
}
