import { ComponentDtoBase } from '@/components/submodules/base/component.dto.base';
import { IsInt, IsPositive } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class HardDriveDto extends ComponentDtoBase {
    @IsPositive()
    @IsInt()
    @ApiProperty({
        name: 'storageCapacity',
        description: 'Объем данных хранилища',
        required: true,
    })
    public storageCapacity: number;

    @IsPositive()
    @IsInt()
    @ApiProperty({
        name: 'rotationSpeed',
        description: 'Скорость вращения',
        required: true,
    })
    public rotationSpeed: number;
}
