import { ComponentsServiceBase } from '@/components/submodules/base/components.service.base';
import { HardDriveEntity } from '@/components/submodules/hardDrives/hardDrive.entity';
import { HardDriveDto } from '@/components/submodules/hardDrives/hardDrive.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class HardDrivesService extends ComponentsServiceBase<
    HardDriveEntity,
    HardDriveDto
> {
    protected componentName = 'Hard Drive';

    constructor(
        @InjectRepository(HardDriveEntity)
        protected readonly repository: Repository<HardDriveEntity>,
    ) {
        super();
    }
}
