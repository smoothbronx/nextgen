import { ComponentEntityBase } from '@/components/submodules/base/component.entity.base';
import { Column, Entity } from 'typeorm';

@Entity('hard_drives')
export class HardDriveEntity extends ComponentEntityBase {
    @Column({ name: 'storage_capacity', nullable: false })
    public storageCapacity: number;

    @Column({ name: 'rotation_speed', nullable: false })
    public rotationSpeed: number;
}
