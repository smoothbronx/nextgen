import { ComponentsControllerBase } from '@/components/submodules/base/components.controller.base';
import { HardDrivesService } from '@/components/submodules/hardDrives/hardDrives.service';
import { HardDriveEntity } from '@/components/submodules/hardDrives/hardDrive.entity';
import { HardDriveDto } from '@/components/submodules/hardDrives/hardDrive.dto';
import { Controller, Inject } from '@nestjs/common';

@Controller('/components/hards/')
export class HardDrivesController extends ComponentsControllerBase<
    HardDriveEntity,
    HardDriveDto
> {
    constructor(
        @Inject(HardDrivesService)
        protected readonly service: HardDrivesService,
    ) {
        super();
    }
}
