import { ComponentsControllerBase } from '@/components/submodules/base/components.controller.base';
import { VideoCardsService } from '@/components/submodules/videoCards/videoCards.service';
import { VideoCardEntity } from '@/components/submodules/videoCards/videoCard.entity';
import { VideoCardDto } from '@/components/submodules/videoCards/videoCard.dto';
import { Controller, Inject } from '@nestjs/common';

@Controller('/components/video/')
export class VideoCardsController extends ComponentsControllerBase<
    VideoCardEntity,
    VideoCardDto
> {
    constructor(
        @Inject(VideoCardsService)
        protected readonly service: VideoCardsService,
    ) {
        super();
    }
}
