import { ComponentEntityBase } from '@/components/submodules/base/component.entity.base';
import { Column, Entity } from 'typeorm';

@Entity('video_cards')
export class VideoCardEntity extends ComponentEntityBase {
    @Column({ name: 'video_memory', nullable: false })
    public videoMemory: number;

    @Column({ name: 'clock_speed', nullable: false })
    public clockSpeed: number;
}
