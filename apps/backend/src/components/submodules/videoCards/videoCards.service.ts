import { ComponentsServiceBase } from '@/components/submodules/base/components.service.base';
import { VideoCardEntity } from '@/components/submodules/videoCards/videoCard.entity';
import { VideoCardDto } from '@/components/submodules/videoCards/videoCard.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class VideoCardsService extends ComponentsServiceBase<
    VideoCardEntity,
    VideoCardDto
> {
    protected readonly componentName = 'Video card';

    constructor(
        @InjectRepository(VideoCardEntity)
        protected readonly repository: Repository<VideoCardEntity>,
    ) {
        super();
    }
}
