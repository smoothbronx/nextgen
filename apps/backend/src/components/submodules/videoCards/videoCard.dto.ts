import { ComponentDtoBase } from '@/components/submodules/base/component.dto.base';
import { IsInt, IsPositive } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class VideoCardDto extends ComponentDtoBase {
    @IsInt()
    @IsPositive()
    @ApiProperty({
        name: 'videoMemory',
        description: 'Объем видеопамяти',
        required: true,
    })
    public videoMemory: number;

    @IsInt()
    @IsPositive()
    @ApiProperty({
        name: 'clockSpeed',
        description: 'Частота видеокарты',
        required: true,
    })
    public clockSpeed: number;
}
