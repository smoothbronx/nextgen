import { PowerSupplyDto } from '@/components/submodules/powerSupplies/powerSupply.dto';
import { MotherboardDto } from '@/components/submodules/motherboards/motherboard.dto';
import { HardDriveDto } from '@/components/submodules/hardDrives/hardDrive.dto';
import { ProcessorDto } from '@/components/submodules/processors/processor.dto';
import { VideoCardDto } from '@/components/submodules/videoCards/videoCard.dto';
import { RamMemoryDto } from '@/components/submodules/ramMemory/ramMemory.dto';
import { IsArray, IsOptional } from 'class-validator';
import { Components } from '@/components/Components';
import { ApiProperty } from '@nestjs/swagger';

export class ComponentsDto {
    @ApiProperty({
        readOnly: true,
        required: false,
        nullable: true,
        isArray: true,
        type: HardDriveDto,
    })
    public hards?: HardDriveDto[];

    @ApiProperty({
        readOnly: true,
        required: false,
        nullable: true,
        isArray: true,
        type: MotherboardDto,
    })
    public motherboards?: MotherboardDto[];

    @ApiProperty({
        readOnly: true,
        required: false,
        nullable: true,
        isArray: true,
        type: ProcessorDto,
    })
    public processors?: ProcessorDto[];

    @ApiProperty({
        readOnly: true,
        required: false,
        nullable: true,
        isArray: true,
        type: PowerSupplyDto,
    })
    public power?: PowerSupplyDto[];

    @ApiProperty({
        readOnly: true,
        required: false,
        nullable: true,
        isArray: true,
        type: RamMemoryDto,
    })
    public ram?: RamMemoryDto[];

    @ApiProperty({
        readOnly: true,
        required: false,
        nullable: true,
        isArray: true,
        type: VideoCardDto,
    })
    public video?: VideoCardDto[];
}

export class ComponentsTypesDto {
    @ApiProperty({
        enum: Components,
        writeOnly: true,
        required: false,
        isArray: true,
        example: Object.values(Components),
    })
    @IsOptional()
    @IsArray()
    public readonly types?: Components[];
}
