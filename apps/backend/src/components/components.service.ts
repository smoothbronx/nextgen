import { PowerSupplyEntity } from '@/components/submodules/powerSupplies/powerSupply.entity';
import { MotherboardEntity } from '@/components/submodules/motherboards/motherboard.entity';
import { HardDriveEntity } from '@/components/submodules/hardDrives/hardDrive.entity';
import { ProcessorEntity } from '@/components/submodules/processors/processor.entity';
import { VideoCardEntity } from '@/components/submodules/videoCards/videoCard.entity';
import { RamMemoryEntity } from '@/components/submodules/ramMemory/ramMemory.entity';
import { ComponentsDto, ComponentsTypesDto } from '@/components/components.dto';
import { BadRequestException, Injectable } from '@nestjs/common';
import { Components } from '@/components/Components';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ComponentsService {
    constructor(
        @InjectRepository(MotherboardEntity)
        private readonly motherboardsRepository: Repository<MotherboardEntity>,
        @InjectRepository(PowerSupplyEntity)
        private readonly powerSuppliesRepository: Repository<PowerSupplyEntity>,
        @InjectRepository(HardDriveEntity)
        private readonly hardDrivesRepository: Repository<HardDriveEntity>,
        @InjectRepository(ProcessorEntity)
        private readonly processorsRepository: Repository<ProcessorEntity>,
        @InjectRepository(RamMemoryEntity)
        private readonly ramMemoriesRepository: Repository<RamMemoryEntity>,
        @InjectRepository(VideoCardEntity)
        private readonly videoCardsRepository: Repository<VideoCardEntity>,
    ) {}

    public async getComponents(
        components: ComponentsTypesDto,
    ): Promise<ComponentsDto> {
        if (!components.types || components.types.length === 0) {
            return this.getAllComponents();
        }

        const result = {};
        for (const component of components.types) {
            result[component] = await this.getComponentsByType(component);
        }

        return result;
    }

    public async getAllComponents(): Promise<ComponentsDto> {
        const result = {};

        for (const component of Object.values(Components)) {
            result[component] = await this.getComponentsByType(component);
        }

        return result;
    }

    private getComponentsByType(type: Components) {
        const repository = this.getRepositoryByComponentType(type);
        return repository.find();
    }

    private getRepositoryByComponentType(name: Components) {
        let repository: Repository<any>;

        if (name === Components.HARD_DRIVE) {
            repository = this.hardDrivesRepository;
        } else if (name === Components.MOTHERBOARD) {
            repository = this.motherboardsRepository;
        } else if (name === Components.POWER_SUPPLY) {
            repository = this.powerSuppliesRepository;
        } else if (name === Components.PROCESSOR) {
            repository = this.processorsRepository;
        } else if (name === Components.RAM_MEMORY) {
            repository = this.ramMemoriesRepository;
        } else if (name === Components.VIDEO_CARD) {
            repository = this.videoCardsRepository;
        } else {
            throw new BadRequestException('Incorrect component type');
        }

        return repository;
    }
}
