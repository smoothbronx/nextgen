import { PowerSuppliesController } from '@/components/submodules/powerSupplies/powerSupplies.controller';
import { MotherboardsController } from '@/components/submodules/motherboards/motherboards.controller';
import { PowerSuppliesService } from '@/components/submodules/powerSupplies/powerSupplies.service';
import { RamMemoriesController } from '@/components/submodules/ramMemory/ramMemories.controller';
import { HardDrivesController } from '@/components/submodules/hardDrives/hardDrives.controller';
import { ProcessorsController } from '@/components/submodules/processors/processors.controller';
import { VideoCardsController } from '@/components/submodules/videoCards/videoCards.controller';
import { MotherboardsService } from '@/components/submodules/motherboards/motherboards.service';
import { PowerSupplyEntity } from '@/components/submodules/powerSupplies/powerSupply.entity';
import { MotherboardEntity } from '@/components/submodules/motherboards/motherboard.entity';
import { RamMemoriesService } from '@/components/submodules/ramMemory/ramMemories.service';
import { VideoCardsService } from '@/components/submodules/videoCards/videoCards.service';
import { HardDrivesService } from '@/components/submodules/hardDrives/hardDrives.service';
import { ProcessorsService } from '@/components/submodules/processors/processors.service';
import { HardDriveEntity } from '@/components/submodules/hardDrives/hardDrive.entity';
import { ProcessorEntity } from '@/components/submodules/processors/processor.entity';
import { VideoCardEntity } from '@/components/submodules/videoCards/videoCard.entity';
import { RamMemoryEntity } from '@/components/submodules/ramMemory/ramMemory.entity';
import { ComponentsController } from '@/components/components.controller';
import { ComponentsService } from '@/components/components.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';

const componentsTypeormModule = TypeOrmModule.forFeature([
    MotherboardEntity,
    PowerSupplyEntity,
    HardDriveEntity,
    ProcessorEntity,
    RamMemoryEntity,
    VideoCardEntity,
]);

const componentsControllers = [
    PowerSuppliesController,
    MotherboardsController,
    RamMemoriesController,
    HardDrivesController,
    ProcessorsController,
    VideoCardsController,
];

const componentsServices = [
    PowerSuppliesService,
    MotherboardsService,
    RamMemoriesService,
    HardDrivesService,
    ProcessorsService,
    VideoCardsService,
];

@Module({
    imports: [componentsTypeormModule],
    controllers: [ComponentsController, ...componentsControllers],
    providers: [...componentsServices, ComponentsService],
    exports: [componentsTypeormModule],
})
export class ComponentsModule {}
