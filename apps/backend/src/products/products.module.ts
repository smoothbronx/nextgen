import { ProductsController } from '@/products/products.controller';
import { StaticModule } from '@/staticfiles/staticfiles.module';
import { ProductsService } from '@/products/products.service';
import { ProductEntity } from '@/products/product.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';

const dynamicTypeormModule = TypeOrmModule.forFeature([ProductEntity]);

@Module({
    imports: [dynamicTypeormModule, StaticModule],
    providers: [ProductsService],
    controllers: [ProductsController],
    exports: [dynamicTypeormModule],
})
export class ProductsModule {}
