import { AssemblyEntity } from '@/assemblies/entities/assembly.entity';
import { Categories } from '@/products/Categories';
import { Expose } from 'class-transformer';
import {
    PrimaryGeneratedColumn,
    BaseEntity,
    JoinColumn,
    ManyToOne,
    Column,
    Entity,
} from 'typeorm';

@Entity('products')
export class ProductEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ name: 'name', nullable: false, type: 'varchar' })
    public name: string;

    @Column({ name: 'description', nullable: true, type: 'varchar' })
    public description: string | null;

    @Column({ name: 'category', enum: Categories, nullable: false })
    public category: Categories;

    @Column({ name: 'picture', type: 'varchar', nullable: false })
    public picture: string;

    @Column({ name: 'quality', nullable: false })
    public quality: number;

    @JoinColumn({ name: 'assembly_id' })
    @ManyToOne(() => AssemblyEntity, {
        onDelete: 'CASCADE',
        nullable: false,
        eager: true,
    })
    public assembly: AssemblyEntity;

    @Expose({ toPlainOnly: true, name: 'price' })
    public price() {
        return this.assembly.price();
    }
}
