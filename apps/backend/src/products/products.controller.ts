import { ProductsService } from '@/products/products.service';
import { ProductEntity } from '@/products/product.entity';
import { ProductDto } from '@/products/dto/product.dto';
import { FilterDto } from '@/products/dto/filter.dto';
import {
    ParseIntPipe,
    UploadedFile,
    Controller,
    Delete,
    Inject,
    Param,
    Body,
    Post,
    Get,
    UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('/products/')
export class ProductsController {
    constructor(
        @Inject(ProductsService)
        private readonly productsService: ProductsService,
    ) {}

    @Post('/filter')
    public getFilteredProducts(
        @Body() filter: FilterDto,
    ): Promise<ProductEntity[]> {
        return this.productsService.getFilteredProducts(filter);
    }

    @Get('/:productId/')
    public getProduct(
        @Param('productId', ParseIntPipe) id: number,
    ): Promise<ProductEntity> {
        return this.productsService.getProduct(id);
    }

    @UseInterceptors(FileInterceptor('file'))
    @Post('/')
    public async createProduct(
        @UploadedFile() file: Express.Multer.File,
        @Body() productDto: ProductDto,
    ): Promise<void> {
        await this.productsService.createProduct(file, productDto);
    }

    @Delete('/:productId/')
    public async deleteProduct(
        @Param('productId', ParseIntPipe) id: number,
    ): Promise<void> {
        return this.productsService.deleteProduct(id);
    }
}
