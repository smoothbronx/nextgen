export enum Categories {
    WORKSPACES = 'workspaces',
    STREAMING = 'streaming',
    GAMING = 'gaming',
}
