import { AssemblyDto } from '@/assemblies/assembly.dto';
import { Categories } from '@/products/Categories';
import { ApiProperty } from '@nestjs/swagger';
import {
    IsOptional,
    IsPositive,
    IsString,
    IsEnum,
    IsInt,
} from 'class-validator';

export class ProductDto {
    @ApiProperty({
        name: 'id',
        description: 'Уникальный идентификатор товара',
        nullable: false,
        readOnly: true,
    })
    public id: number;

    @IsString()
    @ApiProperty({
        name: 'name',
        description: 'Название продукта',
        nullable: false,
        required: true,
    })
    public name: string;

    @IsOptional()
    @IsString()
    @ApiProperty({
        name: 'description',
        description: 'Описание продукта',
        required: false,
        nullable: true,
    })
    public description?: string;

    @ApiProperty({
        name: 'price',
        description: 'Цена продукта',
        nullable: false,
        readOnly: true,
    })
    public price: number;

    @IsEnum(Categories)
    @ApiProperty({
        name: 'category',
        description: 'Категория продукта',
        enum: Categories,
        nullable: false,
        required: true,
    })
    public category: Categories;

    @ApiProperty({
        name: 'picture',
        description: 'Путь до картинки',
        nullable: false,
        readOnly: true,
        required: true,
    })
    public picture: string;

    @IsInt()
    @IsPositive()
    @ApiProperty({
        name: 'quantity',
        description: 'Количество товара',
        nullable: false,
        required: true,
    })
    public quality: number;

    @IsInt()
    @IsPositive()
    @ApiProperty({
        name: 'assemblyId',
        description: 'Идентификатор сборки, к которой привязан продукт',
        required: true,
        writeOnly: true,
    })
    public assemblyId: number;

    @ApiProperty({
        name: 'assembly',
        description: 'Объект сборки, к которой привязан продукт',
        nullable: false,
        readOnly: true,
    })
    public assembly: AssemblyDto;
}
