import { Categories } from '@/products/Categories';
import { ApiProperty } from '@nestjs/swagger';
import {
    IsOptional,
    IsPositive,
    IsString,
    IsEnum,
    IsInt,
} from 'class-validator';

export class FilterDto {
    @IsOptional()
    @IsString()
    @ApiProperty({
        name: 'name',
        description: 'Имя продукта',
        required: false,
        writeOnly: true,
        nullable: true,
    })
    public name?: string;

    @IsOptional()
    @IsEnum(Categories)
    @ApiProperty({
        name: 'category',
        required: false,
        nullable: true,
        writeOnly: true,
        enum: Categories,
    })
    public category?: Categories;

    @IsOptional()
    @IsInt()
    @IsPositive()
    @ApiProperty({
        name: 'processorId',
        required: false,
        nullable: true,
        writeOnly: true,
    })
    public processorId?: number;

    @IsOptional()
    @IsInt()
    @IsPositive()
    @ApiProperty({
        name: 'videoCardId',
        required: false,
        nullable: true,
        writeOnly: true,
    })
    public videoCardId?: number;
}
