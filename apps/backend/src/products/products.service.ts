import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { StaticService } from '@/staticfiles/staticfiles.service';
import { ProductEntity } from '@/products/product.entity';
import { ProductDto } from '@/products/dto/product.dto';
import { FilterDto } from '@/products/dto/filter.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';

@Injectable()
export class ProductsService {
    constructor(
        @InjectRepository(ProductEntity)
        private readonly productsRepository: Repository<ProductEntity>,
        @Inject(StaticService)
        private readonly staticService: StaticService,
    ) {}

    public getFilteredProducts(filter: FilterDto): Promise<ProductEntity[]> {
        return this.productsRepository.findBy({
            name: filter.name ? Like(filter.name) : undefined,
            category: filter.category,
            assembly: {
                processor: { id: filter.processorId },
                videoCard: { id: filter.videoCardId },
            },
        });
    }

    public async getProduct(productId: number): Promise<ProductEntity> {
        const product = await this.productsRepository.findOneById(productId);

        if (!product) {
            throw new NotFoundException('Product not found');
        }

        return product;
    }

    public async createProduct(
        file: Express.Multer.File,
        productDto: ProductDto,
    ) {
        const product = this.productsRepository.create({
            assembly: { id: productDto.assemblyId },
            description: productDto.description,
            category: productDto.category,
            quality: productDto.quality,
            name: productDto.name,
        });

        product.picture = (await this.staticService.uploadFile(file)).path;
        return product.save();
    }

    public async deleteProduct(productId: number): Promise<void> {
        const product = await this.getProduct(productId);
        await product.remove();
    }
}
